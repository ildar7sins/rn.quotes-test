import {Dimensions, Platform} from 'react-native';

const win = Dimensions.get('window');
export const width = win.height;
export const height = win.width;

export const BASE_URL = 'https://poloniex.com/'

// Константы для стилей...
export const FONT_DEFAULT_HEIGHT = Platform.OS === 'ios' ? 414 : 335;
