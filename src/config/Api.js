import {Alert} from 'react-native';
import {BASE_URL} from '../config/constants';
import {rainbowMsgStyle} from '../config/console-style';
import axios from 'react-native-axios';
import {UPDATE_QUOTE_ERROR, UPDATE_QUOTES} from '../types';


/**
 * Get запрос с авторизацией и обработкой ответа и ошибок...
 * @param fetchUrl
 * @param dataObject
 * @returns {Promise<boolean|*>}
 */
export const apiGet = async (fetchUrl, dataObject = {}, dispatch = null) => {
	
	// ToDo: add internet connection...
	
	// Здесь размещаем работу с хедерами (авторизация и прочее)...
	// const headers = await getHeaders();
	const headers = {};
	
	console.log('Url: ', `${BASE_URL}${fetchUrl}`, 'Тело запроса: ', dataObject, 'headers: ', headers);
	
	let result = await axios.get(
		`${BASE_URL}${fetchUrl}`,
		{
			params: dataObject,
			headers,
			timeout: 30000
		}
	).then(response => {
		return response.data;
	}).catch((error) => {
		console.log(error);
		return false;
	});
	
	console.log('%c%s', rainbowMsgStyle, 'Ответ от сервера: ', result);
	if (result) {
		dispatch({
			type: UPDATE_QUOTE_ERROR,
			payload: null
		});
		
		return result;
	} else {
		if(dispatch){
			// ToDo: additional error handling...
			
			dispatch({
				type: UPDATE_QUOTE_ERROR,
				payload: 'Не удалось загрузить данные'
			});
		}
		
		return false;
	}
}
