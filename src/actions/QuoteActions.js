import {UPDATE_QUOTES} from "../types";
import {apiGet} from "../config/Api";

export const updateQuotes = () => async dispatch => {
	const result = await apiGet('public', {command: 'returnTicker'}, dispatch);
	
	if(result){
		await dispatch({
			type: UPDATE_QUOTES,
			payload: result
		});
	}
	
	return true;
};
