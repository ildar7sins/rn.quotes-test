import React, { useState } from 'react';
import {
	View, FlatList,
	StyleSheet, Text,
	SafeAreaView, TouchableOpacity, ActivityIndicator
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import {width, height, FONT_DEFAULT_HEIGHT} from './../../config/constants';
import { RFValue } from 'react-native-responsive-fontsize';
import {useDispatch, useSelector} from 'react-redux';
import OneQuote from './components/OneQuote';
import {updateQuotes} from '../../actions';
import { useFocusEffect } from '@react-navigation/native';


const QuotePage = ({navigation}) => {
	const dispatch = useDispatch();
	const quotes = useSelector(state => state.quotes.quotes);
	const error = useSelector(state => state.quotes.error);
	const firstLoad = useSelector(state => state.quotes.firstLoad);
	const tableOptions = {
		headers: {
			name: 'Name',
			last: 'Last',
			highestBid: 'Bid',
			percentChange: '% change'},
		columnsWidth: [25, 26, 25, 24]
	}
	
	
	useFocusEffect(
		React.useCallback(() => {
			console.log('focus');
			dispatch(updateQuotes());
			
			const intervalID = setInterval(() => {
				dispatch(updateQuotes());
			}, 5000)
			
			
			return () => {
				clearTimeout(intervalID)
				console.log('unfocus');
			};
		}, [error])
	);
	
	const quotesKeys = Object.keys(quotes);
	
	return (
		<SafeAreaView style={container}>
			<View style={header}>
				<TouchableOpacity onPress={() => navigation.goBack(null)}>
					<Text>{"< Назад"}</Text>
				</TouchableOpacity>
			</View>
			{error ? (
				<Text style={errorMessage}>{error}</Text>
			) : null}
			<View style={table}>
				<OneQuote name={tableOptions.headers.name} quote={tableOptions.headers} rowWidth={tableOptions.columnsWidth}/>
				<FlatList
					data={quotesKeys}
					renderItem={({item, index}) => <OneQuote name={item} quote={quotes[item]} rowWidth={tableOptions.columnsWidth}/>}
					keyExtractor={item => item}
					refreshing={false}
					onRefresh={() => dispatch(updateQuotes())}
					ListEmptyComponent={() => {
						return firstLoad ? null : (
							<ActivityIndicator style={tableLoader} size="small" color="#2d2d2d"/>
						)
					}}
				/>
			</View>
			
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems:'center',
		backgroundColor: '#f1f1f1',
	},
	header: {
		width: '95%',
		marginHorizontal: '2.5%',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-start',
		marginBottom: hp('2.5%')
	},
	errorMessage: {
		color: 'darkred',
		marginVertical: 7
	},
	table: {
		width: '95%',
		flex: 1,
		marginHorizontal: '2.5%',
		borderWidth: 1,
		borderColor: 'darkgrey',
		marginBottom: 15,
	},
	tableLoader: {
		marginTop: 20
	}
});

const {
	container, header, table, errorMessage, tableLoader
} = styles

export default QuotePage;
