import {useSelector} from 'react-redux';
import {FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import React from 'react';
import {RFValue} from 'react-native-responsive-fontsize';
import {FONT_DEFAULT_HEIGHT} from '../../../config/constants';

const OneQuote = ({name, quote, rowWidth}) => {
	return (
		<View style={container}>
			<View style={[cell, {width: `${rowWidth[0]}%`}]}>
				<Text style={cellText}>{name}</Text>
			</View>
			<View style={[cell, {width: `${rowWidth[1]}%`}]}>
				<Text style={cellText}>{quote.last}</Text>
			</View>
			<View style={[cell, {width: `${rowWidth[2]}%`}]}>
				<Text style={cellText}>{quote.highestBid}</Text>
			</View>
			<View style={[cell, lastCell, {width: `${rowWidth[3]}%`}]}>
				<Text style={cellText}>{quote.percentChange}</Text>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		width: '100%',
		flexDirection: 'row',
		borderBottomColor: 'darkgrey',
		borderBottomWidth: 1,
		alignItems:'center',
		backgroundColor: '#f1f1f1'
	},
	cell: {
		paddingVertical: 5,
		paddingLeft: 5,
		borderRightColor: 'darkgrey',
		borderRightWidth: 1,
		height: 40
	},
	lastCell: {
		borderRightWidth: 0
	},
	cellText: {
		fontSize: RFValue(6, FONT_DEFAULT_HEIGHT),
	}
});

const {
	container, cell, lastCell, cellText
} = styles

export default OneQuote;
