import React from 'react'
import {
	View,
	TouchableOpacity,
	StyleSheet, Text,
	SafeAreaView,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import {FONT_DEFAULT_HEIGHT} from './../../config/constants';
import { RFValue } from 'react-native-responsive-fontsize';
import {QUOTE_PAGE} from '../../routes';

const InfoPage = ({navigation}) => {
	
	return (
		<SafeAreaView style={container}>
			<Text style={title}>О приложении</Text>
			<Text style={description}>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit.
				Architecto debitis ipsam ipsum iusto magnam odit, quae quisquam.
				Adipisci, distinctio dolorem earum, enim eum exercitationem facere porro quod saepe unde veniam.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit.
				Architecto debitis ipsam ipsum iusto magnam odit, quae quisquam.
				Adipisci, distinctio dolorem earum, enim eum exercitationem facere porro quod saepe unde veniam.
			</Text>
			<TouchableOpacity style={pageBtn} onPress={() => navigation.navigate(QUOTE_PAGE)}>
				<Text style={pageBtnText}>Котировки</Text>
			</TouchableOpacity>
		</SafeAreaView>)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#f1f1f1'
	},
	scrollWrapper: {
		height: 480,
		marginTop: hp(8)
	},
	title: {
		textAlign: 'center',
		fontSize: RFValue(9, FONT_DEFAULT_HEIGHT),
		fontWeight: 'bold',
		marginTop: hp('1.5%'),
		marginBottom: hp('3.5%')
	},
	description: {
		fontSize: RFValue(7, FONT_DEFAULT_HEIGHT),
		paddingHorizontal: wp('5%'),
		marginBottom: hp('3.5%')
	},
	pageBtn: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#7376FF',
		borderRadius: 4,
		height: 36,
		width: '47.5%'
	},
	pageBtnText: {
		color: '#ffffff',
		fontSize: RFValue(9, FONT_DEFAULT_HEIGHT),
		fontWeight: 'bold'
	},
})


const {container, title, description, pageBtn, pageBtnText} = styles;

export default InfoPage;
