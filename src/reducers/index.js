import {combineReducers} from 'redux';
import QuotesReducer from './QuotesReducer';

export default combineReducers({
	quotes: QuotesReducer
});
