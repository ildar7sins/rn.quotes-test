import {UPDATE_QUOTES, UPDATE_QUOTE_ERROR} from '../types';

const INITIAL_STATE = {
	quotes: {},
	firstLoad: false,
	error: null
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case UPDATE_QUOTES:
			return {
				...state,
				quotes: action.payload,
				firstLoad: true
			};
		case UPDATE_QUOTE_ERROR:
			return {
				...state,
				error: action.payload,
				firstLoad: true
			};
		default:
			return state;
	}
};
