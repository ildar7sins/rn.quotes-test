import {connect, Provider} from 'react-redux';
import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import InfoPage from './pages/Info/InfoPage';
import QuotePage from './pages/Quote/QuotePage';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {INFO_PAGE, QUOTE_PAGE} from './routes'

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const InfoTab = () => {
	return (
		<Tab.Navigator>
				<Tab.Screen name="О приложении" component={InfoPage} />
		</Tab.Navigator>
	);
};
const QuoteTab = () => {
	return (
		<Tab.Navigator>
			<Tab.Screen name="Котировки" component={QuotePage} />
		</Tab.Navigator>
	);
};

class App extends Component {
	constructor(props) {
		super(props);
	}
	
	render() {
		return (
			
			<NavigationContainer>
				<Drawer.Navigator
					initialRouteName="Info"
					drawerContentOptions={{
						activeTintColor: 'blue',
						inactiveTintColor: 'grey',
					}}>
					<Drawer.Screen
						name={INFO_PAGE}
						component={InfoTab}
						options={{drawerLabel: 'О приложении'}}
					/>
					<Drawer.Screen
						name={QUOTE_PAGE}
						component={QuoteTab}
						options={{drawerLabel: 'Котировки'}}
					/>
				</Drawer.Navigator>
			</NavigationContainer>
		);
	}
}


const mapStateToProps = state => ({});

export default connect(
	mapStateToProps,
	{},
)(App);
