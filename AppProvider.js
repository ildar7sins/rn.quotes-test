import React, {Component} from 'react';

import App from './src/index';
import {Provider} from 'react-redux';
import configureStore from './src/store/Configuration';

export default class AppProvider extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const store = configureStore();
    
    return (
        <Provider store={store}>
          <App />
        </Provider>
    );
  }
}
